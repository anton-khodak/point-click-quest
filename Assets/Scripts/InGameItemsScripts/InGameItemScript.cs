﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine;
public enum GameObjectState{
	Active,
	Inactive,
	Inaccessible
};

public enum GameObjectType {
	Item,
	Object,
	Door
};
[Serializable]
public class Condition{
	public List<InGameItemScript> condition;
}
[Serializable]
public class InGameItemScript : MonoBehaviour {
	public List<Condition> ConditionToBecomeActive;
	public List<InGameItemScript> ObjectToMakeInaccessible;
	public List<InGameItemScript> ObjectToMakeActive;
	public GameObjectState State;
	public bool IsInteracted = false;
	public GameObjectType Type;
	public int SceneIndex;
	private MovementScript MainCharacter;
	private Interact GameController;
	public string Message;
		
	void OnMouseDown() {
		if (MainCharacter == null)
			MainCharacter = GameObject.Find ("MainCharacter").GetComponent<MovementScript> ();
		if (this.State == GameObjectState.Active)
			MainCharacter.TryToInteract (this.gameObject);
	}

	public void Interact() {
		if (Type == GameObjectType.Door)
			SceneManager.LoadScene (SceneIndex);
		else {
			IsInteracted = true;
			foreach (InGameItemScript item in ObjectToMakeInaccessible)
				item.BecomeInaccessible ();
			foreach (InGameItemScript item in ObjectToMakeActive)
				item.CheckState ();
			if (GameController == null)
				GameController = GameObject.Find ("GameController").GetComponent<Interact> ();
		}
	}

	public void CheckState() {
		if (ConditionToBecomeActive.Any (condition => condition.condition.All (obj => obj.IsInteracted)))
			this.State = GameObjectState.Active;
	}

	public void BecomeInaccessible() {
		this.State = GameObjectState.Inaccessible;
	}
}
