﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;



public class PauseScript : MonoBehaviour {

	public Transform PauseCanvas;
	public Transform InGameCanvas;
	private GameObject MainCharacter;
	private GameObject Controller;
	public Canvas MessageBox;
	[HideInInspector]
	public bool IsGameSaved = false;

	void Start(){
		MainCharacter = GameObject.Find ("MainCharacter");
		Controller = GameObject.Find ("GameStateController");
	}

	public void Pause() {
		PauseCanvas.gameObject.SetActive (true);
		InGameCanvas.gameObject.SetActive (false);
		SetIsGamePaused (true);
		Time.timeScale = 0;
	}

	public void SetIsGamePaused(bool _flag) {
		MainCharacter.GetComponent<MovementScript> ().IsGamePaused = _flag;
	}


	public void Unpause() {
		PauseCanvas.gameObject.SetActive (false);
		InGameCanvas.gameObject.SetActive (true);
		SetIsGamePaused (false);
		IsGameSaved = false;
		Time.timeScale = 1;
	}

	public void ExitToMainMenu(){
		if (!IsGameSaved) {
			PauseCanvas.gameObject.SetActive (false);
			Canvas dialog = Instantiate (MessageBox);
			MessageBoxInterface settings = dialog.GetComponentInChildren<MessageBoxInterface> ();
			settings.SetActionsAndMessage (SaveAndQuit, "If You quit without saving the game,\nYou will lose all unsaved progress." +
				"\nDo You want to save the game before exiting?", Quit);
		} else {
			Quit ();
		}
	}

	public void Save(){
		IsGameSaved = true;
		Controller.GetComponent<GameState> ().Save (MainCharacter.GetComponent<MovementScript>());
	}

	private void SaveAndQuit() {
		Save ();
		Quit ();
	}

	private void Quit() {
		Time.timeScale = 1;
		SceneManager.LoadScene (0);
	}
}
