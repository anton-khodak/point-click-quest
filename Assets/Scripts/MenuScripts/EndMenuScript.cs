﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndMenuScript : MonoBehaviour {

	// Use this for initialization
	public void GoToMainMenu(){
		SceneManager.LoadScene (0);
	}
}
