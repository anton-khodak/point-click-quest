﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBoxInterface : MonoBehaviour {

	// Use this for initialization
	public delegate void Action();
	public Canvas Parent;
	private Action YesAnswer;
	private Action NoAnswer;
	public Text TextBox;
	public void SetActionsAndMessage(Action _yesAction, string _message, Action _noAction = null) {
		YesAnswer = _yesAction;
		TextBox.text = _message;
		NoAnswer = _noAction;
	}

	public void Yes() {
		YesAnswer ();
		Close ();
	}

	public void No() {
		if (NoAnswer != null)
			NoAnswer ();
		Close ();
	}

	public void Close() {
		Destroy (Parent.gameObject);
	}
}
