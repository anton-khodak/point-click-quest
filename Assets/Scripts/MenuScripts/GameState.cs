﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameInfo {
	public int SceneIndex;
	public float CharacterPosition;
	public bool CharacterDirection;
	public void SetInfo(int _sceneIndex, float _characterPosition, bool _characterDirection) {
		SceneIndex = _sceneIndex;
		CharacterDirection = _characterDirection;
		CharacterPosition = _characterPosition;
	}
}

public class GameState : MonoBehaviour {
	private GameInfo State;
	public string Status;
	void Start(){
		State = new GameInfo ();
	}

	public void Save (MovementScript _character){
		MovementScript characterMovementInfo = _character.GetComponent<MovementScript> ();
		State.SetInfo (SceneManager.GetActiveScene ().buildIndex, _character.transform.position.x,
			characterMovementInfo.Direction);
		PlayerPrefs.SetString("SavedGame", JsonUtility.ToJson (State));
		PlayerPrefs.Save ();
	}

	public void Load(MovementScript _character) {
		Vector3 position = _character.transform.position;
		_character.transform.position = new Vector3(State.CharacterPosition, position.y, position.z);
		_character.Direction = State.CharacterDirection;
	}

	public void SetGameInfo(GameInfo _state) {
		State = _state;
	}
}
