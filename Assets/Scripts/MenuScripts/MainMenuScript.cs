﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour {

	// Use this for initialization
	public Canvas Menu;
	public Button LoadButton;
	string SavedGame;
	private static GameObject GameSaver;
	public Canvas MessageBox;
	void Awake() {
		if (GameSaver == null) {
			GameSaver = GameObject.Find ("GameStateController");
			DontDestroyOnLoad (GameSaver);
		}
	}

	void Start(){
		SavedGame = PlayerPrefs.GetString ("SavedGame", string.Empty);
		if (!string.IsNullOrEmpty (SavedGame))
			LoadButton.interactable = true;
	}
	
	public void LoadGame(){
		GameInfo state = JsonUtility.FromJson <GameInfo> (SavedGame);
		SceneManager.LoadScene (state.SceneIndex);
		GameState controller = GameSaver.GetComponent<GameState> ();
		controller.SetGameInfo (state);
		controller.Status = "Loaded";
	}

	public void Quit () {
		
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit ();
		#endif
	}

	public void StartNewGame()
	{
		if (LoadButton.interactable) {
			Menu.gameObject.SetActive (false);
			Canvas dialog = Instantiate (MessageBox);
			MessageBoxInterface settings = dialog.GetComponentInChildren<MessageBoxInterface> ();
			settings.SetActionsAndMessage (CreateNewGame, 
				"By starting new game you will delete all progress.\nDo You want to start new game?"
				, MakeMenuActive);
		} else {
			CreateNewGame();
		}
	}

	private void CreateNewGame() {
		PlayerPrefs.DeleteKey ("SavedGame");
		GameSaver.GetComponent<GameState> ().Status = "NewGame";
		SceneManager.LoadScene (1);
	}

	private void MakeMenuActive() {
		Menu.gameObject.SetActive (true);
	}
		
}
