﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

	[SerializeField] List<Item> startingItems;
	[SerializeField] Transform itemsParent;
	[SerializeField] ItemSlot[] itemSlots;

	private void OnValidate()
	{
		if (itemsParent != null)
			itemSlots = itemsParent.GetComponentsInChildren<ItemSlot>();

		SetStartingItems();
	}

	private void SetStartingItems()
	{
		int i = 0;
		for (; i < startingItems.Count && i < itemSlots.Length; i++)
		{
			itemSlots[i].Item = startingItems[i];
		}

		for (; i < itemSlots.Length; i++)
		{
			itemSlots[i].Item = null;
		}
	}
}
