﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interact : MonoBehaviour {

	// Use this for initialization
	public Image InteractBox;
	private InteractWindowInfo windowDetails;
	private InGameItemScript InteractedObject;

	void Awake() {
		windowDetails = InteractBox.GetComponent<InteractWindowInfo> ();
	}

	public void SetObject(InGameItemScript _object) {
		InteractedObject = _object;
		if (_object.State == GameObjectState.Active) {
			if (_object.Type != GameObjectType.Door) {
				string buttonText;
				if (_object.Type == GameObjectType.Item)
					buttonText = "Pick up.";
				else
					buttonText = "Okay";
				windowDetails.SetMessageAndButtonText (_object.Message, buttonText);
				GetComponent<PauseScript> ().SetIsGamePaused (true);
				InteractBox.gameObject.SetActive (true);
			}
			_object.Interact ();
		}
	}

	public void CloseBox() {
		GetComponent<PauseScript> ().SetIsGamePaused (false);
		InteractBox.gameObject.SetActive (false);
	}
}
