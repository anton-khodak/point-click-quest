﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractWindowInfo : MonoBehaviour {

	public Text ConfirmButtonText;
	public Text Message;

	public void SetMessageAndButtonText(string _message, string _buttonText) {
		ConfirmButtonText.text = _buttonText;
		Message.text = _message;
	}
}
