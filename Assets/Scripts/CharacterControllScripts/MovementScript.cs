﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementScript : MonoBehaviour {

	public float MovementSpeed = 5f;
	public float MovementLag = 0.1f;
	private bool IsMovementInProgress = false;
	private Interact interactionSystem;
	private GameObject ObjectToMove;
	[HideInInspector]
	private bool direction;
	public bool Direction {// false - left, true - right
		get { return direction; }
		set {
			direction = value;
			SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
			renderer.flipX = !direction;
		}
	}
	private float destination;
	public bool IsGamePaused = false;
	public Camera MainCamera;
	private bool interactionFlag = false;

	void Awake() {
		interactionSystem = GameObject.Find ("GameController").GetComponent<Interact> ();
	}

	void Start() {
		GameState controller = GameObject.Find ("GameStateController").GetComponent<GameState> ();
		if (controller.Status == "Loaded") {
			controller.Load (this);
		}
	}

	void Update () {
		Vector3 mousePosition = MainCamera.ScreenToWorldPoint (
			new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
		if (Input.GetMouseButtonDown (0) && !IsGamePaused && mousePosition.y >= -3f && mousePosition.y <= 3.5f) {
			SetMovementDetails (mousePosition);
			if (!interactionFlag)
				ObjectToMove = null;
		}
		if (IsMovementInProgress)
			Move ();
		interactionFlag = false;
	}

	public void SetMovementDetails(Vector3 _destinationCoordinates) {
		if (Mathf.Abs (_destinationCoordinates.x - transform.position.x) > MovementLag) {
			IsMovementInProgress = true;
			Direction = _destinationCoordinates.x > transform.position.x;
			destination = _destinationCoordinates.x + (Direction ? -1 : 1) * MovementLag;
		}
	}
	public float InteractDistance = 3f;

	public void TryToInteract(GameObject _object) {
		interactionFlag = true;
		ObjectToMove = _object;
		if (Mathf.Sqrt (Mathf.Pow (transform.position.x - _object.transform.position.x, 2) +
		    Mathf.Pow (transform.position.y - _object.transform.position.y, 2)) < InteractDistance)
			this.Interact ();

	}

	private void Move() {
		float step =  (Direction ? 1 : -1)* MovementSpeed * Time.deltaTime;
		if (ObjectToMove == null && Mathf.Abs (transform.position.x + step - destination) < MovementLag || 
			!direction && transform.position.x + step < destination || direction && transform.position.x + step > destination) {
			transform.position = new Vector3 (destination, transform.position.y, transform.position.z);
			IsMovementInProgress = false;
		} else if (ObjectToMove != null && Mathf.Sqrt (Mathf.Pow (transform.position.x - ObjectToMove.transform.position.x, 2) +
		           Mathf.Pow (transform.position.y - ObjectToMove.transform.position.y, 2)) < InteractDistance) {
			IsMovementInProgress = false;
			Interact ();
		} else
			transform.position = new Vector3 (transform.position.x + step, transform.position.y, transform.position.z);
	}

	private void Interact() {
		interactionSystem.SetObject (ObjectToMove.GetComponent<InGameItemScript> ());
		ObjectToMove = null;
	}
}
